import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

#importing dataset
batches=[]
batches.append(pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\data_batch_1'))
batches.append(pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\data_batch_2'))
batches.append(pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\data_batch_3'))
batches.append(pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\data_batch_4'))
batches.append(pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\data_batch_5'))
batches.append(pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\data_batch_6'))

#viewing the dictionary keys
#print(batches[5].keys())

for i in range(6):
	print(batches[i][b'batch_label'])

#here we see that batch 1-5 are training and 6 is testing dataset
#now examining the label dictionary
batchmeta=pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\batches.meta')
labels={}
for x in range(10):
    labels[x] = batchmeta['label_names'][x]
print(labels)

#now need to split training and test dataset
xtrain=[]
xtest=[]
ytrain=[]
for i in range(5):
	batch=batches[i]
	for j in range(10000):
		xtrain.append(batch[b'data'][j])
		ytrain.append(batch[b'labels'][j])
xtrain=np.array(xtrain)
ytrain=np.array(ytrain)

#(hot encoding) now converting the ytrain from a number i from 1-10 to a vector of shape 1x10
#in which index i has value 1 and others have value 0
ytrain2=[]
for i in range(50000):
	arr=[0]*10
	x=ytrain[i]
	arr[x]=1
	ytrain2.append(arr)
ytrain2=np.array(ytrain2)

#normalising data so the value of a pixel lies between 0-1 instead of 0-255
xtrain=xtrain.astype(np.float64)
ytrain=ytrain.astype(np.float64)
for i in range(50000):
	xtrain[i]=xtrain[i]/255
m=50000

#starting the training

def categorical_cross_entropy_cost(h):
	y=ytrain2
	loss=np.sum(y * (-1 * np.log(h)) +(1 - y) * (-1 * np.log(1 - h)),axis=0)
	print(f"Loss shape:{loss.shape}")
	# return (1/m)*np.sum(np.sum(y * (-1 * np.log(h)) +(1 - y) * (-1 * np.log(1 - h))))
	return loss

def mse(h):
	return np.mean(np.power(ytrain2-h,2))

def mse_derivative(h):
	return 2*(h-ytrain2)/ytrain2.size

def sigmoid_forward(z):
  return np.maximum(z, 0)

def sigmoidGradient(z):
    return (z>0).astype(z.dtype)

def softmax(h):
    exps = np.exp(h - np.max(h, axis=1, keepdims=True))
    return exps/np.sum(exps, axis=1, keepdims=True)


# def softmax_backward(z):
# 	np.clip(z,(10**(-3)),(10**3))
# 	z = np.reshape(z, (10,1))
# 	softmax = softmax_forward(z)
# 	softmax = np.reshape(softmax, (1, -1))
# 	d_softmax = (softmax * np.identity(softmax.size)- softmax.transpose() @ softmax)
# 	input_grad = z @ d_softmax
# 	return input_grad



#forwardprop
w1 = np.random.randn(1024, 128)
w2 = np.random.randn(128, 10)
thet1=(np.array([[0.00001]*1024]*128)).T
thet2=(np.array([[0.00001]*128]*10)).T

grad1 = np.zeros((w1.shape))
grad2 = np.zeros((w2.shape))
learning_rate=0.01

for epoch in range(20):
	x=xtrain
	input=np.array(x)
	# print(input.shape)
	input=input.reshape(50000,1024)
	# print()
	dense1=input.dot(w1)
	# print(f"dense1 {dense1.shape}")
	act_d1=sigmoid_forward(dense1)
	# print(f"activation dense1 {act_d1.shape}")
	dense2=act_d1.dot(w2)
	# print(f"dense2 {dense2.shape}")
	# print(dense2[0:3])
	act_d2=softmax_forward(dense2)
	# print(f"A4 {act_d2.shape}")
	# print(act_d2)
	output=act_d2
	# print(output[0:3])
	count=0
	for i in range(50000):
		k=np.argmax(output[i])
		if k==ytrain[i]:
			count=count+1
	accuracy=(count/50000)*100
	loss=mse(act_d2)
	print(f"epochs {epoch}: loss {loss}: Accuracy {accuracy}")
	#backprop
	loss2=mse_derivative(act_d2)
	d2=act_d2-ytrain2
	gradw2=(1./m)*np.dot(act_d1.T,d2)
	d1=np.dot(d2,w2.T)*sigmoidGradient(act_d1)
	gradw1=(1./m)*np.dot(x.T, d1)
	w1=w1-learning_rate*gradw1
	w2=w2-learning_rate*gradw2

# testing

x=batches[5]
input=np.array(x)
# print(input.shape)
input=input.reshape(10000,1024)
# print()
dense1=input.dot(w1)
# print(f"dense1 {dense1.shape}")
act_d1=sigmoid_forward(dense1)
# print(f"activation dense1 {act_d1.shape}")
dense2=act_d1.dot(w2)
# print(f"dense2 {dense2.shape}")
act_d2=softmax(dense2)
# print(f"A4 {act_d2.shape}")
# print(act_d2)
output=act_d2
for i in range(10000):
	k=np.argmax(output[i])
	if k==ytrain[i]:
		count=count+1
accuracy=(count/10000)*100
loss=categorical_cross_entropy_cost(act_d2)
print(f"epochs {epoch}: loss {loss}: Accuracy {accuracy}")
