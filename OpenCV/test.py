import cv2
import numpy as np
import matplotlib.pyplot as plt

cap = cv2.VideoCapture(r'C:\Users\Maximus\CSE\challenge_videoeasy.mp4')
img=cap[120]
def region_of_interest(img):
    height=img.shape[0]
    width=img.shape[1]
    height -= 50
    width -= 50
    Polygons = np.array([
        [(width, height), (50,height), (int((3/8)*width), int((3/4)*height)),(int((5/8)*width), int((3/4)*height))]
            (width, height-100), (width,height//2), (0,height//2), (0,height-100)
        [([(int((3/8)*width), height), (int((5/8)*width), height), (int((1/2)*width),int((3/4)*height))])],
        []])
    mask = np.zeros_like(img)
    cv2.fillConvexPoly(mask, Polygons, 255)
    maskimg = cv2.bitwise_and(img, mask)
    return maskimg
plt.imshow(region_of_interest(img))
