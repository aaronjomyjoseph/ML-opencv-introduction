import matplotlib.pyplot as plt
import numpy as np
from silence_tensorflow import silence_tensorflow
silence_tensorflow()
import tensorflow as tf
import tensorflow_datasets as tfds
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Flatten
from keras.callbacks import EarlyStopping


#importing the data
imgdata, label = tfds.as_numpy(tfds.load('eurosat',split='train',batch_size=-1,as_supervised=True,))

#splitting the data
x_train,x_test,y_train,y_test=train_test_split(imgdata,label,test_size=0.2,random_state=123)
plt.imshow
#hot encoding
y_train2=[]
y_test2=[]
trainsize=y_train.shape[0]
testsize=y_test.shape[0]

for i in range(trainsize):
	arr=[0]*10
	x=y_train[i]
	arr[x]=1
	y_train2.append(arr)
y_train2=np.array(y_train2) 

for i in range(testsize):
	arr=[0]*10
	x=y_test[i]
	arr[x]=1
	y_test2.append(arr)
y_test2=np.array(y_test2)
y_train=y_train2
y_test=y_test2
print(y_test[0])
print(y_test2[0])


#data augmentation
augmented_train= ImageDataGenerator(
    rescale=1./255,
    featurewise_center = True,
    featurewise_std_normalization = True,
    rotation_range = 55,
    width_shift_range = 0.2,
    height_shift_range = 0.2,
    shear_range = 0.2,
    zoom_range = 0.2,
    horizontal_flip = True,
    vertical_flip = True,
    brightness_range=[0.2,0.8]
    )

augmented_test = ImageDataGenerator(rescale=1./255)

train = augmented_train.flow(x_train, y_train, batch_size=64)
test = augmented_test.flow(x_test, y_test, batch_size=64)

model = Sequential()

model.add(Conv2D(32, (3,3), activation='relu', input_shape = (64, 64, 3)))
model.add(MaxPooling2D((2,2)))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D((2, 2)))
model.add(Conv2D(128, (3, 3), activation='relu'))
model.add(MaxPooling2D((2, 2)))

model.add(Flatten())

model.add(Dense(64, activation='relu')) 
model.add(Dense(10, activation='softmax'))

model.summary()


model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
steps=len(train)
history = model.fit(train, steps_per_epoch=steps, epochs=200, callbacks=None)
_, acc = model.evaluate_generator(test, steps=len(test), verbose=2)
