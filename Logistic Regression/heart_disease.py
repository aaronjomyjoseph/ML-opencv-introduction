import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


data = pd.read_csv (r'C:\Users\Maximus\manastaskphase_aaron1\data\heartdisease.csv')
data.dropna(inplace=True) #dropping na values
data.drop(['education'], axis=1, inplace=True) #removing education as the feature  poses little significance to heart disease prediction
m=data['male'].count() #deriving the number of training cases
l=[1.0]*m
data['new']=l # adding X0=1 column
X=data.to_numpy()
X=X.astype(np.float64)
print(X)
y=X[:,14] #extracting the heartdisease value y(=0/1)
m=len(y)
X=np.delete(X, 14, 1)
thet=np.array([[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1]])
Xsc=X.copy()
minx=np.min(X[:,1:])
maxx=np.max(X[:,1:])
Xsc[:,1:]=(X[:,1:]-minx)/(maxx-minx)
ysc=y.copy()
ysc=ysc.reshape(-1,1)
def sigmoid(z):
    return 1 / (1 + np.exp(-z))
def grad(error):
    pd = (np.dot(Xsc.T, error))/m
    return pd
def cost(h):
    return (1/m)*np.sum(y * (-1 * np.log(h)) +(1 - y) * (-1 * np.log(1 - h)))
J_history = []
epoch_history=[]
def regression(theta0,learning_rate=1,epochs=150):
    print(f'epoch \t Cost(J) \t')
    for epoch in range(epochs):
        epoch_history.append(epoch)
        ypr=Xsc.dot(theta0)
        h=sigmoid(ypr).reshape(-1,1)
        error=h-ysc
        dJ=grad(error)
        J=cost(h)
        theta0=theta0-learning_rate*dJ
        J_history.append(J)
        if epoch%10==0:
            print(f'{epoch:5d}\t{J_history[-1]:7.4f}\t')
    return theta0
newtheta=regression(thet)
ypr=Xsc.dot(newtheta)
h=sigmoid(ypr).reshape(-1,1)
i=0
for x in h:
    if x>=0.5:
        ypr[i]=1
    elif x<0.5:
        ypr[i]=0
    i=i+1
i=0
right=0
for i in range(m):
    if ypr[i]==ysc[i]:
        right=right+1
acc=(right/m)*100
print("accuracy",acc)
plt.plot(epoch_history,J_history)
plt.show()
