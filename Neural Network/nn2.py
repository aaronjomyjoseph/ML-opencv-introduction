
import pandas as pd
import numpy as np

#importing dataset
batches=[]
batches.append(pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\data_batch_1'))
batches.append(pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\data_batch_2'))
batches.append(pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\data_batch_3'))
batches.append(pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\data_batch_4'))
batches.append(pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\data_batch_5'))
batches.append(pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\data_batch_6'))

#viewing the dictionary keys
print(batches[5].keys())

for i in range(6):
	print(batches[i][b'batch_label'])

#here we see that batch 1-5 are training and 6 is testing dataset
#now examining the label dictionary
batchmeta=pd.read_pickle(r'C:\Users\Maximus\manastaskphase_aaron1\data\cifar-10-batches\batches.meta')
labels={}
for x in range(10):
    labels[x] = batchmeta['label_names'][x]
print(labels)

#now need to split training and test dataset
xtrain=[]
xtest=[]
ytrain=[]
for i in range(5):
	batch=batches[i]
	for j in range(10000):
		xtrain.append(batch[b'data'][j])
		ytrain.append(batch[b'labels'][j])
xtrain=np.array(xtrain)
ytrain=np.array(ytrain)

#(hot encoding) now converting the ytrain from a number i from 1-10 to a vector of shape 1x10
#in which index i has value 1 and others have value 0
ytrain2=[]
for i in range(50000):
	arr=[0]*10
	x=ytrain[i]
	arr[x]=1
	ytrain2.append(arr)
ytrain2=np.array(ytrain2)

#normalising data so the value of a pixel lies between 0-1 instead of 0-255
xtrain=xtrain.astype(np.float64)
ytrain=ytrain.astype(np.float64)
for i in range(50000):
	xtrain[i]=xtrain[i]/255

def tanh(x):
    return np.tanh(x);

def tanh_prime(x):
    return 1-np.tanh(x)**2;




def sigmoid(s):
    return 1/(1 + np.exp(-s))

def sigmoid_derv(s):
    return s * (1 - s)

def softmax(s):
    exps = np.exp(s - np.max(s, axis=1, keepdims=True))
    return exps/np.sum(exps, axis=1, keepdims=True)

def cross_entropy(pred, real):
    n_samples = real.shape[0]
    res = pred - real
    return res/n_samples

def error(pred, real):
    n_samples = real.shape[0]
    logp = - np.log(pred[np.arange(n_samples), real.argmax(axis=1)])
    loss = np.sum(logp)/n_samples
    return loss

class MyNN:
    def __init__(self, x, y):
        self.x = x
        neurons = 32
        self.lr = 0.01
        ip_dim = x.shape[1]
        op_dim = y.shape[1]

        self.w1 = np.random.randn(ip_dim, neurons)
        self.b1 = np.zeros((1, neurons))
        self.w2 = np.random.randn(neurons, neurons)
        self.b2 = np.zeros((1, neurons))
        self.w3 = np.random.randn(neurons, op_dim)
        self.b3 = np.zeros((1, op_dim))
        self.y = y
        self.loss=0
    def feedforward(self):
        z1 = np.dot(self.x, self.w1) + self.b1
        self.a1 = tanh(z1)
        z2 = np.dot(self.a1, self.w2) + self.b2
        self.a2 = tanh(z2)
        z3 = np.dot(self.a2, self.w3) + self.b3
        self.a3 = softmax(z3)
        
    def backprop(self):
        self.loss = error(self.a3, self.y)
        # print('Error :', loss)
        a3_delta = cross_entropy(self.a3, self.y) # w3
        z2_delta = np.dot(a3_delta, self.w3.T)
        a2_delta = z2_delta * tanh_prime(self.a2) # w2
        z1_delta = np.dot(a2_delta, self.w2.T)
        a1_delta = z1_delta * tanh_prime(self.a1) # w1

        self.w3 = self.w3-self.lr * np.dot(self.a2.T, a3_delta)
        self.b3 = self.b3-self.lr * np.sum(a3_delta, axis=0, keepdims=True)
        self.w2 = self.w2-self.lr * np.dot(self.a1.T, a2_delta)
        self.b2 = self.b2-self.lr * np.sum(a2_delta, axis=0)
        self.w1 = self.w1-self.lr * np.dot(self.x.T, a1_delta)
        self.b1 = self.b1-self.lr * np.sum(a1_delta, axis=0)

    def predict(self, data):
        self.x = data
        self.feedforward()
        return self.a3.argmax()
			
model = MyNN(xtrain,ytrain2)

epochs = 2500
for x in range(epochs):
    model.feedforward()
    # if x%10==0:
    print(f'{x:5d}\t{model.loss}')
    model.backprop()
		
def get_acc(x, y):
    acc = 0
    for xx,yy in zip(x, y):
        s = model.predict(xx)
        if s == np.argmax(yy):
            acc=acc+1
    return acc/len(x)*100
	
print("Training accuracy : ", get_acc(xtrain, ytrain))
# print("Test accuracy : ", get_acc(x_val/16, np.array(y_val)))